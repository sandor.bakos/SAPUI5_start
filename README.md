A Project based upon a SAPUI5 book "The Comprehensive Guide"

# 2017.12.21

- index.html file with JSON in the scriptline.
- It is a simple table app without any MVC model

# 2017.12.24

- folder structure
- data.json file
- mvc pattern / half way (jsview and controller js done, need componentjs setting the model and manifest?)

# 2017.12.25

- plus detail page
- controller works

- The program use an MVC pattern, with JS view.
- It has a very simple feature, It is capable to show certain deatils of a company in a new view

# 2017.12.27

- js view changed to xml view
- Master.component.js file fixed.

# 2017.12.30

- creating and using the component js file

# 2017.12.31

- Table responsive margin class
- App in Shell
- new data.json file in the service folder
- component.js modified
 - metadata property

# 2018.01.01

- routing

# 2018.01.02

- routing fixed
- manifest.json added
- component.js modified according to the manifest.json
- propery binding
- formatter.js
- types.js
