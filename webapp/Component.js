sap.ui.define([
//using the module loading syntax of the sapui5 - it loads these modules asyncrounosly
//AMD asyncrounos module definition
//requiring the UIComponent component from the different libraries
  "sap/ui/core/UIComponent",
], function(UIComponent){
  "use strict";

  return UIComponent.extend("sapui5.start.Component", {
  // we extend the UIComponent and name it to the "sapui5.start.Component"
    metadata: {
      manifest: "json"
    },

    init: function(){
      //call the base component init function
      UIComponent.prototype.init.apply(this, arguments);
      //create the views based on the URL/hash
      this.getRouter().initialize();
    }
  });
});
